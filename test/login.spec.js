import { mount } from '@vue/test-utils'
import Login from '../src/views/user/Login.vue'


describe('Login', () => {
    // Now mount the component and you have the wrapper
    const wrapper = mount(Login)
  
    it('renders the correct markup', () => {
      expect(wrapper.html()).toContain('<span class="count">0</span>')
    })
  
    // it's also easy to check for the existence of elements
    it('has a button', () => {
      expect(wrapper.contains('button')).toBe(true)
    })
  })