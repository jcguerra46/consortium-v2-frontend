import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  /**
   * Get Professional functions List
   * @param auth
   * @returns {Promise<*>}
   */
  getProfessionalFunctionsList: async (auth) => {
    try {
      let response = await Axios.get(`${BASEURL}/professional-functions`, auth);
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },
};
