import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  /**
   * store a new resource.
   *
   * @param auth
   * @param consortiumData
   * @returns {Promise<void>}
   */
  createConsortium: async (auth, consortiumData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/consortium`,
        consortiumData,
        auth
      );
      let consortium = response.data.data;
      return consortium;
    } catch (error) {
      throw error;
    }
  },

  /**
   * show a specified resource.
   *
   * @param auth
   * @param idConsortium
   * @returns {Promise<void>}
   */
  getConsortium: async (auth, idConsortium) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}`,
        auth
      );
      let consortium = response.data.data;
      return consortium;
    } catch (error) {
      throw error;
    }
  },

  /**
   * show a specified cashbox resource.
   *
   * @param auth
   * @param idConsortium
   * @returns {Promise<void>}
   */
  getConsortiumCashbox: async (auth, idConsortium) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/cashbox`,
        auth
      );
      let consortium = response.data.data;
      return consortium;
    } catch (error) {
      throw error;
    }
  },

  /**
   * show a list of resources.
   *
   * @param auth
   * @returns {Promise<void>}
   */
  getListConsortium: async (auth) => {
    try {
      let response = await Axios.get(`${BASEURL}/consortium`, auth);
      let consortium = response.data.data;
      return consortium;
    } catch (error) {
      console.log(error.response);
      throw error;
    }
  },

  /**
   * Update a specified resource.
   *
   * @param auth
   * @param idConsortium
   * @param consortiumData
   * @returns {Promise<void>}
   */
  editConsortium: async (auth, idConsortium, consortiumData) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/consortium/${idConsortium}`,
        consortiumData,
        auth
      );
      let consortium = response.data.data;
      return consortium;
    } catch (error) {
      console.log("ERR", error);
      console.log("ERR", error.response.data.message);
      throw error;
    }
  },

  /**
   * Update the status of the resource.
   *
   * @param auth
   * @param idConsortium
   * @param state
   * @returns {Promise<void>}
   */
  editStateConsortium: async (auth, idConsortium, state) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/consortium/${idConsortium}/edit-state`,
        state,
        auth
      );
      let consortium = response.data.data;
      return consortium;
    } catch (error) {
      console.log("ERR", error);
      console.log("ERR", error.response.data.message);
      throw error;
    }
  },
};
