import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  /**
   * Store a new resource
   * @param auth
   * @param serviceData
   * @returns {Promise<*>}
   */
  createService: async (auth, serviceData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/service`,
        serviceData,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Show a specified resource.
   * @param auth
   * @param idService
   * @returns {Promise<*>}
   */
  showService: async (auth, idService) => {
    try {
      let response = await Axios.get(`${BASEURL}/service/${idService}`, auth);
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Update a specified resource.
   * @param auth
   * @param serviceData
   * @param idService
   * @returns {Promise<*>}
   */
  editService: async (auth, serviceData, idService) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/service/${idService}`,
        serviceData,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Delete a specified resource.
   * @param auth
   * @param idService
   * @returns {Promise<*>}
   */
  deleteService: async (auth, idService) => {
    try {
      let response = await Axios.delete(
        `${BASEURL}/service/${idService}`,
        auth
      );
      let deleteService = response.data.data;

      return deleteService;
    } catch (error) {
      throw error;
    }
  },
};
