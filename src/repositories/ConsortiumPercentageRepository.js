import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Display a listing of the resource.
     *
     * @param auth
     * @param idConsortium
     * @returns {Promise<void>}
     */
    getConsortiumPercentages: async (auth, idConsortium) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/consortium/${idConsortium}/percentage`,
                auth
            );
            let consortiumPercentage = response.data.data;
            return consortiumPercentage;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Store a new resource
     *
     * @param auth
     * @param consortiumPercentageData
     * @param idConsortium
     * @returns {Promise<void>}
     */
    storeConsortiumPercentage: async (auth, consortiumPercentageData, idConsortium) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/consortium/${idConsortium}/percentage`,
                consortiumPercentageData,
                auth
            );
            let consortiumPercentage = response.data.data;
            return consortiumPercentage;
        } catch (error) {
            throw error
        }
    },

    /**
     * Show a specified resource.
     *
     * @param auth
     * @param idConsortium
     * @param idPercentage
     * @returns {Promise<void>}
     */
    showConsortiumPercentage: async (auth, idConsortium, idPercentage) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/consortium/${idConsortium}/percentage/${idPercentage}`,
                auth
            );
            let consortiumPercentage = response.data.data;
            return consortiumPercentage;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Update a specified resource.
     *
     * @param auth
     * @param idConsortium
     * @param consortiumPercentageData
     * @returns {Promise<void>}
     */
    updateConsortiumPercentage: async (auth, idConsortium, idPercentage, consortiumPercentageData) => {
        try {
            let response = await Axios.put(
                `${BASEURL}/consortium/${idConsortium}/percentage/${idPercentage}`,
                consortiumPercentageData,
                auth
            );
            let consortiumPercentage = response.data.data;
            return consortiumPercentage;
        } catch (error) {
            console.log(error.response);
            throw error;
        }
    },

    /**
     * Store a batch of percentages
     *
     * @param auth
     * @param percentagesData
     * @param consortiumId
     * @returns {Promise<void>}
     */
    createPercentages: async (auth, percentagesData, consortiumId) => {
        try {
            let response = await Axios.patch(
                `${BASEURL}/consortium/${consortiumId}/percentage`,
                { percentages: percentagesData },
                auth
            );
            let percentages = response.data.data;
            return percentages;
        } catch (error) {
            throw error
        }
    },

    /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idConsortium
     * @param idPercentage
     * @returns {Promise<void>}
     */
    destroyConsortiumPercentage: async (auth, idConsortium, idPercentage) => {
        try {
            let response = await Axios.delete(
                `${BASEURL}/consortium/${idConsortium}/percentage/${idPercentage}`,
                auth
            );
            let consortiumPercentage = response.data.data;
            return consortiumPercentage;
        } catch (error) {
            throw error;
        }
    },


};
