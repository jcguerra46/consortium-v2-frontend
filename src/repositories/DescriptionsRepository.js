import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Store a new resource
     *
     * @param auth
     * @param descriptionData
     * @returns {Promise<*>}
     */
    createDescription: async (auth, descriptionData) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/spending-description`,
                descriptionData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error
        }
    },

    /**
     * Show a specified resource.
     *
     * @param auth
     * @param idDescription
     * @returns {Promise<*>}
     */
    showDescription: async (auth, idDescription) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/spending-description/${idDescription}`,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Update a specified resource.
     *
     * @param auth
     * @param descriptionData
     * @param idDescription
     * @returns {Promise<*>}
     */
    updateDescription: async (auth, descriptionData, idDescription) => {
        try {
            let response = await Axios.put(
                `${BASEURL}/spending-description/${idDescription}`,
                descriptionData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idDescription
     * @returns {Promise<*>}
     */
    destroyDescription: async (auth, idDescription) => {
      try {
          let response = await Axios.delete(
              `${BASEURL}/spending-description/${idDescription}`,
              auth
          );
          return response.data.data;
      }  catch (error) {
          throw error;
      }
    }

}
