import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;



export default {

    getMenu : async (auth) => {
        let response = await Axios.get(`${BASEURL}/menus`, auth);
        return response.data.data.menu;
    }
}