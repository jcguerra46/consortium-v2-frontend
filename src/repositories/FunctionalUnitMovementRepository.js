import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

/**
 * Create a Contact.
 *
 * @param auth
 * @param movementData
 * @param idFU
 * @returns {Promise<*>}
 */

export default {
  createMovement: async (auth, movementData, idFU) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/functional-units/${idFU}/movement`,
        movementData,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },
  /**
   * Show a specified resource.
   *
   * @param auth
   * @param idFU
   * @returns {Promise<*>}
   */
  showDebt: async (idFU, auth) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/functional-units/${idFU}/movement/debts`,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },
};
