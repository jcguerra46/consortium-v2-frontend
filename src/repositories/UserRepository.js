import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;
import { UserModel } from "../models/UserModel";

export default {
  userAdministrations: async (token) => {
    const auth = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    let responseUserAdministration = await Axios.get(
      `${BASEURL}/user-administrations`,
      auth
    );
    let userData = responseUserAdministration.data.data;
    let User = new UserModel({ ...userData, token });
    return User
  },
};
