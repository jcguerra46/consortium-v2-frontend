import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  storeBankAccountMovement: async (
    auth,
    idConsortium,
    idBank,
    movementData
  ) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/consortium/${idConsortium}/bank-account/${idBank}/movement`,
        movementData,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },
};
