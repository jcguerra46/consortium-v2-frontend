import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Get balance
     *
     * @param auth
     * @param idConsortium
     * @returns {Promise<*>}
     */
    getConsortiumMovementBalance: async (auth, idConsortium) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/consortium/${idConsortium}/movement/balance`,
                auth
            );
            let balance = response.data.data;
            return balance;
        } catch (error) {
            console.log(error);
            throw error;
        }
    },

}
