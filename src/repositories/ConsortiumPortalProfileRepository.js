import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  getConsortiumPortalProfile: async (auth, idConsortium) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/portal/profile`,
        auth
      );
      let consortiumProfile = response.data.data;
      return consortiumProfile;
    } catch (error) {
      console.log(
        "CREATE CONSORTIUM PORTAL PROFILE",
        error.response.data.message
      );
      throw error;
    }
  },
  editConsortiumPortalProfile: async (
    auth,
    idConsortium,
    consortiumPortalProfileData
  ) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/consortium/${idConsortium}/portal/profile`,
        consortiumPortalProfileData,
        auth
      );
      let consortiumPortalProfile = response.data.data;
      return consortiumPortalProfile;
    } catch (error) {
      console.log(
        "CREATE CONSORTIUM PORTAL PROFILE",
        error.response.data.message
      );
      throw error;
    }
  },
};
