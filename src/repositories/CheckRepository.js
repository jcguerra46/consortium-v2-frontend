import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Show a specified resource.
     *
     * @param auth
     * @param idCheck
     * @returns {Promise<*>}
     */
    showCheck: async (auth, idCheck) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/check/${idCheck}`,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },


}
