import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  getCashboxMovementBalance: async (auth, idCashbox) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/cashbox/${idCashbox}/balances`,
        auth
      );
      let balance = response.data.data;
      return balance;
    } catch (error) {
      throw error;
    }
  },
  getCashboxMovement: async (auth, idCashbox, idMovement) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/cashbox/${idCashbox}/movement/${idMovement}`,
        auth,
        idCashbox,
        idMovement
      );
      let movement = response.data.data;
      return movement;
    } catch (error) {
      throw error;
    }
  },
};
