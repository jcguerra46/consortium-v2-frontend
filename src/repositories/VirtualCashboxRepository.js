import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  /**
   * Store a specified resource.
   *
   * @param auth
   * @param idConsortium
   * @param cashboxData
   * @returns {Promise<*>}
   */
  storeVirtualCashbox: async (auth, idConsortium, cashboxData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/consortium/${idConsortium}/virtual-cashbox/`,
        cashboxData,
        auth
      );
      let cashboxStored = response.data.data;
      return cashboxStored;
    } catch (error) {
      throw error;
    }
  },
  /**
   * Store a initial balance resource.
   *
   * @param auth
   * @param idConsortium
   * @param cashboxData
   * @returns {Promise<*>}
   */
  storeVirtualCashboxInitialBalance: async (
    auth,
    idConsortium,
    idCashbox,
    initialBalance
  ) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/consortium/${idConsortium}/virtual-cashbox/${idCashbox}/movement/initial-balance`,
        initialBalance,
        auth
      );
      let cashboxStored = response.data.data;
      return cashboxStored;
    } catch (error) {
      console.log(error.response);
      throw error;
    }
  },
  /**
   * Show a specified resource.
   *
   * @param auth
   * @param idConsortium
   * @param idCashbox
   * @returns {Promise<*>}
   */
  showVirtualCashbox: async (auth, idConsortium, idCashbox) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/virtual-cashbox/${idCashbox}`,
        auth
      );
      let cashboxShow = response.data.data;
      return cashboxShow;
    } catch (error) {
      throw error;
    }
  },
  /**
   * Delete a specified resource.
   *
   * @param auth
   * @param idConsortium
   * @param idCashbox
   * @returns {Promise<*>}
   */
  deleteVirtualCashbox: async (auth, idConsortium, idCashbox) => {
    try {
      let response = await Axios.delete(
        `${BASEURL}/consortium/${idConsortium}/virtual-cashbox/${idCashbox}`,
        auth
      );
      let cashboxDelete = response.data.data;
      return cashboxDelete;
    } catch (error) {
      throw error;
    }
  },
};
