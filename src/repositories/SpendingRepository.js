import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Store a new resource
     *
     * @param auth
     * @param spendingData
     * @returns {Promise<*>}
     */
    createSpending: async (auth, spendingData) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/spending`,
                spendingData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error
        }
    },

    /**
     * Show a specified resource.
     *
     * @param auth
     * @param idSpendingRecurrent
     * @returns {Promise<*>}
     */
    showSpending: async (auth, idSpending) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/spending/${idSpending}`,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Update a specified resource.
     *
     * @param auth
     * @param spendingData
     * @param idSpending
     * @returns {Promise<*>}
     */
    updateSpending: async (auth, spendingData, idSpending) => {
        try {
            let response = await Axios.put(
                `${BASEURL}/spending/${idSpending}`,
                spendingData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idSpending
     * @returns {Promise<*>}
     */
    destroySpending: async (auth, idSpending) => {
      try {
          let response = await Axios.delete(
              `${BASEURL}/spending/${idSpending}`,
              auth
          );
          return response.data.data;
      }  catch (error) {
          throw error;
      }
    }

}
