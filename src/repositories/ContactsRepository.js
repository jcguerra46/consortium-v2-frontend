import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

    /**
     * Create a Contact.
     *
     * @param auth
     * @param contactData
     * @param idFU
     * @returns {Promise<*>}
     */

export default {
  createContact: async (auth, contactData, idFU) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/functional-units/${idFU}/contact`,
        contactData,
        auth
      );
      return response.data.data;
    } catch (error) {
        throw error
    }
  },

    /**
     * Update a specified resource.
     *
     * @param auth
     * @param functionalUnitData
     * @param idFU
     * @returns {Promise<*>}
     */
    updateContact: async (auth, contactData, idContact) => {
      try {
          let response = await Axios.put(
              `${BASEURL}/contact/${idContact}`,
              contactData,
              auth
          );
          return response.data.data;
      } catch (error) {
          throw error;
      }
  },

   /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idFU
     * @returns {Promise<*>}
     */
    destroyContact: async (auth, idFU, idContact) => {
      try {
          let response = await Axios.delete(
              `${BASEURL}/functional-units/${idFU}/contact/${idContact}`,
              auth
          );
          return response.data.data;
      }  catch (error) {
          throw error;
      }
    },

   /**
     * Show a specified resource.
     *
     * @param auth
     * @param idContact
     * @returns {Promise<*>}
     */
    showContact: async (auth, idContact) => {
      try {
          let response = await Axios.get(
              `${BASEURL}/contact/${idContact}`,
              auth
          );
          return response.data.data;
      } catch (error) {
          throw error;
      }
  },
};
