import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;
import UserRepository from './UserRepository'

export default {

    login : async (userData) => {
        let responseLogin = await Axios.post(`${BASEURL}/auth/login`, userData);
       
        let token = responseLogin.data.data.token
        return UserRepository.userAdministrations(token);
      },
    logout : async(auth) => {
      await Axios.post(`${BASEURL}/auth/logout`, null, auth);
    },
    isLoggedIn: async(auth) => {
      await Axios.get(`${BASEURL}/auth/user`, auth);
    }
}