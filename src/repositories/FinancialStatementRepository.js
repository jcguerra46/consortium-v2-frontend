import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Store a new resource
     *
     * @param auth
     * @param financialStatementData
     * @param idConsortium
     * @returns {Promise<*>}
     */
    storeFinancialStatementOutflow: async (auth, financialStatementData, idConsortium) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/cashbox/${idConsortium}/outflow`,
                financialStatementData,
                auth
            );
            let financialStatement = response.data.data;
            return financialStatement;
        } catch (error) {
            throw error
        }
    },

        /**
     * Store a new resource
     *
     * @param auth
     * @param financialStatementData
     * @param idConsortium
     * @returns {Promise<*>}
     */
    storeFinancialStatementIncome: async (auth, financialStatementData, idConsortium) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/cashbox/${idConsortium}/income`,
                financialStatementData,
                auth
            );
            let financialStatement = response.data.data;
            return financialStatement;
        } catch (error) {
            throw error
        }
    },

};
