import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  createPercentages: async (auth, percentagesData, consortiumId) => {
    try {
      let response = await Axios.patch(
        `${BASEURL}/consortium/${consortiumId}/percentage`,
        { percentages: percentagesData },
        auth
      );
      let percentages = response.data.data;
      return percentages;
    } catch (error) {
      throw error
    }
  },

  getFunctionalUnitsPercentage: async (auth, consortiumId, percentageId) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${consortiumId}/percentage/${percentageId}/functional-units-percentages`,
        auth
      );
      let percentages = response.data.data;
      return percentages;
    } catch (error) {
      throw error
    }
  },

  assignPercentage: async (
    auth,
    consortiumId,
    percentageId,
    functionalUnits
  ) => {
    try {
      let response = await Axios.patch(
        `${BASEURL}/consortium/${consortiumId}/percentage/${percentageId}/functional-units-percentages`,
        {functional_units: functionalUnits},
        auth
      );
      let percentages = response.data.data;
      return percentages;
    } catch (error) {
      throw error
    }
  },
};
