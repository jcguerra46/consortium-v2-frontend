import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Store a new resource
     *
     * @param auth
     * @param headerData
     * @returns {Promise<*>}
     */
    createHeader: async (auth, headerData) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/expense-headers`,
                headerData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error
        }
    },

    /**
     * Show a specified resource.
     *
     * @param auth
     * @param idHeader
     * @returns {Promise<*>}
     */
    showHeader: async (auth, idHeader) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/expense-headers/${idHeader}`,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Update a specified resource.
     *
     * @param auth
     * @param headerData
     * @param idHeader
     * @returns {Promise<*>}
     */
    updateHeader: async (auth, headerData, idHeader) => {
        try {
            let response = await Axios.put(
                `${BASEURL}/expense-headers/${idHeader}`,
                headerData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idHeader
     * @returns {Promise<*>}
     */
    destroyHeader: async (auth, idHeader) => {
      try {
          let response = await Axios.delete(
              `${BASEURL}/expense-headers/${idHeader}`,
              auth
          );
          return response.data.data;
      }  catch (error) {
          throw error;
      }
    }

}
