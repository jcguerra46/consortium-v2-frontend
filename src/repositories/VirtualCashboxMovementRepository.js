import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  /**
   * Get specified Cash Movement Balance.
   *
   * @param auth
   * @param idConsortium
   * @param idCashbox
   * @returns {Promise<*>}
   */
  getVirtualCashboxMovementBalance: async (auth, idConsortium, idCashbox) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/virtual-cashbox/${idCashbox}/balance`,
        auth
      );
      let balance = response.data.data;
      return balance;
    } catch (error) {
      throw error;
    }
  },
};
