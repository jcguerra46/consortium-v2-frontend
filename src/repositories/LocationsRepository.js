import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  getCountries: async (auth) => {
    let response = await Axios.get(`${BASEURL}/countries`, auth);
    let formatedCountries = response.data.data.map((item) => {
      return {
        value: item.id,
        text: item.name,
      };
    });
    return formatedCountries;
  },

  getProvinces: async (auth, countryId) => {
    let response = await Axios.get(
      `${BASEURL}/countries/${countryId}/provinces`,
      auth
    );
    let formatedProvinces = response.data.data.map((item) => {
      return {
        value: item.id,
        text: item.name,
      };
    });
    return formatedProvinces;
  },
};
