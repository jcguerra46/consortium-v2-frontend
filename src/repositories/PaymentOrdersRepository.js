import Axios from 'axios';

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

  /**
   * show a list of resources.
   *
   * @param auth
   * @returns {Promise<void>}
   */
  getListPaymentOrders: async (auth) => {
    try {
      let response = await Axios.get(`${BASEURL}`, auth);
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Create a payment order
   *
   * @param auth
   * @param paymentOrderData
   * @returns {Promise<*>}
   */
  createPaymentOrder: async (auth, paymentOrderData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/payment-order`,
        paymentOrderData,
        auth,
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  annularPaymentOrder: async (auth, paymentOrderId) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/payment-order/${paymentOrderId}/annular`,
        null,
        auth,
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  showPaymentOrder: async (auth, paymentOrderId) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/payment-order/${paymentOrderId}`,
        auth,
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  doPayment: async (auth, paymentData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/payment-order/${paymentData.id}/do-payment`,
        paymentData,
        auth,
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  updatePaymentOrder: async (auth, paymentOrderData, id) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/payment-order/${id}`,
        paymentOrderData,
        auth,
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

};
