import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  getConsortiumProfile: async (auth, idConsortium) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/profile`,
        auth
      );
      let consortiumProfile = response.data.data;
      return consortiumProfile;
    } catch (error) {
      throw error;
    }
  },
  editConsortiumProfile: async (auth, idConsortium, consortiumProfileData) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/consortium/${idConsortium}/profile`,
        consortiumProfileData,
        auth
      );
      let consortiumProfile = response.data.data;
      return consortiumProfile;
    } catch (error) {
      console.log("CREATE CONSORTIUM PROFILE", error);
      console.log("CREATE CONSORTIUM PROFILE", error.response.data.message);
      throw error;
    }
  },
};
