import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  getFunctionalUnitTypes: async (auth) => {
    let response = await Axios.get(`${BASEURL}/functional-unit-types`, auth);
    let formattedTypes = response.data.data.map((item) => {
      return {
        value: item.id,
        text: item.description,
      };
    });
    return formattedTypes;
  },

};