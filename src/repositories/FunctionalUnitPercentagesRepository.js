import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  /**
   * Show a specified resource.
   *
   * @param auth
   * @param idFU
   * @returns {Promise<*>}
   */
  showPercentages: async (auth, idFU) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/functional-units/${idFU}/percentage/`,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Update a group of resources.
   *
   * @param auth
   * @param percentages
   * @param idFU
   * @returns {Promise<*>}
   */
  updateFunctionalUnitPercentages: async (auth, percentages, idFU) => {
    try {
      let response = await Axios.patch(
        `${BASEURL}/functional-units/${idFU}/percentage`,
        { percentages },
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },
};
