import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  createFunctionalUnits: async (auth, functionalUnitsData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/functional-units/batch`,
        functionalUnitsData,
        auth
      );
      let functionalUnits = response.data.data;
      return functionalUnits;
    } catch (error) {
        throw error
    }
  },

      /**
     * Update a specified resource.
     *
     * @param auth
     * @param functionalUnitData
     * @param idFU
     * @returns {Promise<*>}
     */
    updateFunctionalUnit: async (auth, functionalUnitData, idFU) => {
      try {
          let response = await Axios.put(
              `${BASEURL}/functional-units/${idFU}`,
              functionalUnitData,
              auth
          );
          return response.data.data;
      } catch (error) {
          throw error;
      }
  },

   /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idFU
     * @returns {Promise<*>}
     */
    destroyFunctionalUnit: async (auth, idFU) => {
      try {
          let response = await Axios.delete(
              `${BASEURL}/functional-units/${idFU}`,
              auth
          );
          return response.data.data;
      }  catch (error) {
          throw error;
      }
    },

        /**
     * Show a specified resource.
     *
     * @param auth
     * @param idFU
     * @returns {Promise<*>}
     */
    showFunctionalUnit: async (auth, idFU) => {
      try {
          let response = await Axios.get(
              `${BASEURL}/functional-units/${idFU}`,
              auth
          );
          return response.data.data;
      } catch (error) {
          throw error;
      }
  },
};
