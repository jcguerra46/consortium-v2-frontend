import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;

export default {
  getBankAccount: async (auth, idConsortium, idBank) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/bank-account/${idBank}`,
        auth
      );

      let bankAccount = response.data.data;
      return bankAccount;
    } catch (error) {
      throw error;
    }
  },
  getBankAccountList: async (auth, idConsortium) => {
    try {
      let response = await Axios.get(
        `${BASEURL}/consortium/${idConsortium}/bank-account`,
        auth,
        idConsortium
      );

      let bankAccountList = response.data.data;
      return bankAccountList;
    } catch (error) {
      throw error;
    }
  },
  editBankAccount: async (auth, idConsortium, idBank, data) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/consortium/${idConsortium}/bank-account/${idBank}`,
        data,
        auth
      );

      let bankAccount = response.data.data;
      return bankAccount;
    } catch (error) {
      throw error;
    }
  },
  createBankAccount: async (auth, idConsortium, data) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/consortium/${idConsortium}/bank-account`,
        data,
        auth
      );
      let bankAccountCreate = response.data.data;
      return bankAccountCreate;
    } catch (error) {
      throw error;
    }
  },
  deleteBankAccount: async (auth, idConsortium, idBank) => {
    try {
      let response = await Axios.delete(
        `${BASEURL}/consortium/${idConsortium}/bank-account/${idBank}`,
        auth
      );
      let bankAccountDelete = response.data.data;
      return bankAccountDelete;
    } catch (error) {
      throw error;
    }
  },
};
