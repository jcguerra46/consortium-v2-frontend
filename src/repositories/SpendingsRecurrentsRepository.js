import Axios from "axios";

const BASEURL = process.env.VUE_APP_BASEURL;

export default {

    /**
     * Store a new resource
     *
     * @param auth
     * @param spendingRecurrentData
     * @returns {Promise<*>}
     */
    createSpendingRecurrent: async (auth, spendingRecurrentData) => {
        try {
            let response = await Axios.post(
                `${BASEURL}/spending-recurrent`,
                spendingRecurrentData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error
        }
    },

    /**
     * Show a specified resource.
     *
     * @param auth
     * @param idSpendingRecurrent
     * @returns {Promise<*>}
     */
    showSpendingRecurrent: async (auth, idSpendingRecurrent) => {
        try {
            let response = await Axios.get(
                `${BASEURL}/spending-recurrent/${idSpendingRecurrent}`,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Update a specified resource.
     *
     * @param auth
     * @param spendingRecurrentData
     * @param idSpendingRecurrent
     * @returns {Promise<*>}
     */
    updateSpendingRecurrent: async (auth, spendingRecurrentData, idSpendingRecurrent) => {
        try {
            let response = await Axios.put(
                `${BASEURL}/spending-recurrent/${idSpendingRecurrent}`,
                spendingRecurrentData,
                auth
            );
            return response.data.data;
        } catch (error) {
            throw error;
        }
    },

    /**
     * Delete a specified resource.
     *
     * @param auth
     * @param idSpendingRecurrent
     * @returns {Promise<*>}
     */
    destroySpendingRecurrent: async (auth, idSpendingRecurrent) => {
      try {
          let response = await Axios.delete(
              `${BASEURL}/spending-recurrent/${idSpendingRecurrent}`,
              auth
          );
          return response.data.data;
      }  catch (error) {
          throw error;
      }
    }

}
