import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;
import UserRepository from "./UserRepository";

export default {
  /**
   * Store a new resource
   * @param auth
   * @param providerData
   * @returns {Promise<*>}
   */
  createProvider: async (auth, providerData) => {
    try {
      let response = await Axios.post(
        `${BASEURL}/provider`,
        providerData,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Show a specified resource.
   * @param auth
   * @param idProvider
   * @returns {Promise<*>}
   */
  showProvider: async (auth, idProvider) => {
    try {
      let response = await Axios.get(`${BASEURL}/provider/${idProvider}`, auth);
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Update a specified resource.
   * @param auth
   * @param providerData
   * @param idDescription
   * @returns {Promise<*>}
   */
  editProvider: async (auth, providerData, idProvider) => {
    try {
      let response = await Axios.put(
        `${BASEURL}/provider/${idProvider}`,
        providerData,
        auth
      );
      return response.data.data;
    } catch (error) {
      throw error;
    }
  },

  /**
   * Delete a specified resource.
   * @param auth
   * @param idDescription
   * @returns {Promise<*>}
   */
  deleteProvider: async (auth, idProvider) => {
    try {
      let response = await Axios.delete(
        `${BASEURL}/provider/${idProvider}`,
        auth
      );
      let deleteProvider = response.data.data;

      return deleteProvider;
    } catch (error) {
      throw error;
    }
  },
};
