export default {
    created() {
    
        
      
        if (localStorage.getItem("formStep4")) {
          try {
            this.formStep4 = JSON.parse(localStorage.getItem("formStep4"));
            this.lastStep = true;
          } catch (e) {
            localStorage.removeItem("formStep4");
          }
          try {
            this.consortium = JSON.parse(localStorage.getItem("consortium"));
            this.functionalUnits = JSON.parse(
              localStorage.getItem("functionalUnits")
            );
            this.percentages = JSON.parse(localStorage.getItem("percentages"));
          } catch (error) {
            throw error;
          }
        } else {
          if (localStorage.getItem("formStep1")) {
            try {
              this.formStep1 = JSON.parse(localStorage.getItem("formStep1"));
            } catch (e) {
              localStorage.removeItem("formStep1");
            }
          }
          if (localStorage.getItem("formStep2")) {
            try {
              this.formStep2 = JSON.parse(localStorage.getItem("formStep2"));
            } catch (e) {
              localStorage.removeItem("formStep2");
            }
          }
          if (localStorage.getItem("formStep3")) {
            try {
              this.formStep3 = JSON.parse(localStorage.getItem("formStep3"));
            } catch (e) {
              localStorage.removeItem("formStep3");
            }
          }
        }
      }
}