const BASEURL = process.env.VUE_APP_BASEURL;
import { mapGetters } from "vuex";
import moment from "moment";

export default {
  data() {
    return {
      from: 0,
      to: 0,
      total: 0,
      lastPage: 0,
      items: [],
      idSelect: "",
      apiBase: BASEURL,
      filter: {},
    };
  },

  computed: {
    ...mapGetters({
      user: "currentUser",
    }),

    computedColumns() {
      return [
        ...this.columns,
        {
          name: "__slot:actions",
          title: "Acciones",
          titleClass: "center aligned text-right",
          dataClass: "center aligned text-right",
          width: "5%",
        },
      ];
    },
  },

  methods: {
    getPagination(paginationData) {
      this.from = paginationData.from;
      this.to = paginationData.to;
      this.total = paginationData.total;
      this.lastPage = paginationData.last_page;
      this.items = paginationData.data;
    },
    searchChange(field, value) {
      if (value === null) {
        value = "";
      }
      this.filter[field] = value;
      this.$nextTick(() => {
        this.$refs.datatable.$refs.vuetable.refresh();
      });
    },
    dateFormatter(apiDate) {
      return moment(apiDate, "DD-MM-YYYY").format("DD-MM-YYYY");
    },
    amountFormatter(amount) {
      return amount !== null
        ? this.$n(parseFloat(amount), "currency", "es-AR")
        : "-";
    },
    setFilters(filters) {
      let incomeFilters = filters;
      for (let key in incomeFilters) {
        if (incomeFilters[key] === "" || incomeFilters[key] === null) {
          delete incomeFilters[key];
        }
      }
      this.filter = incomeFilters;
      this.$nextTick(() => {
        this.$refs.datatable.$refs.vuetable.refresh();
      });
    },
  },
};
