export default {
    methods: {
        changePageSize(perPage) {
            this.perPage = perPage;
            this.$nextTick(() => {
                this.$refs.datatable.$refs.vuetable.refresh();
            })
        }
    }
}
