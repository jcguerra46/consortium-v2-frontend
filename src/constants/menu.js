const data = [
  {
    id: "consorcio",
    to: "/app/consortiums",
    fav: true,
    icon: "iconsminds-building",
    label: "consortiums",
  },
  {
    id: "spendings",
    to: "/app/spendings",
    fav: true,
    icon: "iconsminds-coins",
    subs: [
      {
        to: "/app/spendings/list",
        fav: true,
        icon: "iconsminds-financial",
        label: "spending-list",
      },
      {
        to: "/app/spendings/recurrents",
        fav: true,
        icon: "iconsminds-financial",
        label: "recurrents",
      },
      {
        to: "/app/spendings/payment-orders",
        fav: true,
        icon: "iconsminds-billing",
        label: "payment-orders",
      },
      {
        to: "/app/spendings/descriptions",
        fav: true,
        icon: "iconsminds-file-edit",
        label: "descriptions",
      },
      {
        to: "/app/spendings/providers",
        fav: true,
        icon: "iconsminds-shop",
        subs: [
          {
            to: "/app/spendings/providers/list",
            fav: false,
            icon: "iconsminds-business-mens",
            label: "list-providers",
          },
          {
            to: "/app/spendings/providers/current-account",
            fav: false,
            icon: "iconsminds-library",
            label: "current-account",
          },
        ],
        label: "providers",
      },
    ],
    label: "spendings",
  },
  {
    id: "collections",
    to: "/app/collections",
    fav: true,
    icon: "iconsminds-handshake",
    label: "collections",
  },
  {
    id: "salaries",
    to: "/app/salaries",
    fav: true,
    icon: "iconsminds-wallet",
    subs: [
      {
        to: "/app/salaries/employees",
        fav: true,
        icon: "iconsminds-business-man-woman",
        label: "employees",
      },
    ],
    label: "salaries",
  },
  {
    id: "cashbox",
    to: "/app/cashbox",
    fav: true,
    icon: "iconsminds-box-close",
    subs: [
      {
        to: "/app/cashbox/cash-count",
        fav: true,
        icon: "iconsminds-box-with-folders",
        label: "cash-count",
      },
      {
        to: "/app/cashbox/financial-statement",
        fav: true,
        icon: "iconsminds-library",
        label: "financial-statement",
      },
      {
        to: "/app/cashbox/others-cashboxes",
        fav: true,
        icon: "iconsminds-safe-box",
        label: "others-cashboxes",
      },
      {
        to: "/app/cashbox/checks",
        fav: true,
        icon: "iconsminds-shop",
        subs: [
          {
            to: "/app/cashbox/checks/checkbooks",
            fav: true,
            icon: "iconsminds-password-field",
            label: "checkbook",
          },
        ],
        label: "checks",
      },
      {
        to: "/app/cashbox/bank-accounts",
        fav: true,
        icon: "iconsminds-bank",
        label: "bank-accounts",
      },
    ],
    label: "cashbox",
  },
  {
    id: "functionalUnits",
    to: "/app/functional-units",
    fav: true,
    icon: "iconsminds-home",
    subs: [
      {
        to: "/app/functional-units/list",
        fav: true,
        icon: "iconsminds-library",
        label: "list",
      },
      {
        to: "/app/functional-units/current-account",
        fav: true,
        icon: "iconsminds-library",
        label: "current-account",
      },
      {
        to: "/app/functional-units/contacts",
        fav: true,
        icon: "iconsminds-male-female",
        label: "contacts",
      },
      {
        to: "/app/functional-units/claims",
        fav: true,
        icon: "iconsminds-danger",
        label: "claims",
      },
    ],
    label: "functional-units",
  },
  {
    id: "reports",
    to: "/app/reports",
    fav: true,
    icon: "iconsminds-pen",
    subs: [
      {
        to: "/app/reports/others",
        fav: true,
        icon: "iconsminds-notepad",
        label: "others",
      },
      {
        to: "/app/reports/user-cashboxes",
        fav: true,
        icon: "iconsminds-box-close",
        label: "user-cashboxes",
      },
      {
        to: "/app/reports/audit",
        fav: true,
        icon: "iconsminds-books",
        label: "audit",
      },
    ],
    label: "reports",
  },
  {
    id: "configurations",
    to: "/app/configurations",
    fav: false,
    icon: "iconsminds-gears",
    subs: [
      {
        to: "/app/configurations/periods",
        fav: false,
        icon: "simple-icon-calendar",
        label: "periods",
      },
      {
        to: "/app/configurations/headers",
        fav: false,
        icon: "simple-icon-docs",
        label: "headers",
      },
      {
        to: "/app/configurations/notes",
        fav: false,
        icon: "iconsminds-notepad",
        label: "notes",
      },
      {
        to: "/app/configurations/trials",
        fav: false,
        icon: "iconsminds-student-hat",
        label: "trials",
      },
      {
        to: "/app/configurations/categories",
        fav: false,
        icon: "simple-icon-list",
        label: "categories",
      },
      {
        to: "/app/configurations/services",
        fav: false,
        icon: "simple-icon-wrench",
        label: "services",
      },
    ],
    label: "configurations",
  },
];

export default data;
