export const categoryConsortiaOptions = [
  { value: "1", text: "1er Categoria" },
  { value: "2", text: "2er Categoria" },
  { value: "3", text: "3er Categoria" },
  { value: "4", text: "4er Categoria" },
];

export const countries = [{ value: 1, text: "Argentina" }];

export const provinces = [
  { value: 1, text: "Capital Federal" },
  { value: 2, text: "Provincia de Buenos Aires" },
  { value: 3, text: "Catamarca" },
  { value: 4, text: "Chaco" },
  { value: 5, text: "Chubut" },
  { value: 6, text: "Córdoba" },
  { value: 7, text: "Corrientes" },
  { value: 8, text: "Entre Ríos" },
  { value: 9, text: "Formosa" },
  { value: 10, text: "Jujuy" },
  { value: 11, text: "La Pampa" },
  { value: 12, text: "La Rioja" },
  { value: 13, text: "Mendoza" },
  { value: 14, text: "Misiones" },
  { value: 15, text: "Neuquen" },
  { value: 16, text: "Río Negro" },
  { value: 17, text: "Salta" },
  { value: 18, text: "San Juan" },
  { value: 19, text: "San Luis" },
  { value: 20, text: "Santa Cruz" },
  { value: 21, text: "Santa Fe" },
  { value: 22, text: "Santiago del Estero" },
  { value: 23, text: "Tierra del Fuego" },
  { value: 24, text: "Tucumán" },
];

export const functionalUnitTypes = [
  { value: 1, text: "Departamento" },
  { value: 2, text: "Cochera" },
  { value: 3, text: "Subsuelo" },
  { value: 4, text: "UC" },
  { value: 5, text: "SUM" },
  { value: 6, text: "Porteria" },
  { value: 7, text: "Local" },
  { value: 8, text: "Baulera" },
  { value: 9, text: "Azotea" },
  { value: 10, text: "Cama" },
  { value: 11, text: "Lote" },
];

export const functionalUnitLegalStateOptions = [
  { value: "trial", text: "Juicio" },
  { value: "agreement", text: "Convenio" },
  { value: "lawyer", text: "Abogado" },
];

export const functionalUnitForgiveInterestOptions = [
  { value: null, text: "No" },
  { value: "this_liquidation", text: "En esta liquidación" },
  { value: "until_further_notice", text: "Hasta nuevo aviso" },
];
export const roundingTypeOptions = [
  { value: "fu", text: "Redondeo y numero UF" },
  { value: "yes", text: "Redondeo" },
  { value: "no", text: "Sin redondeo" },
];

export const liquidationOptions = [
  { value: "vencido", text: "Mes vencido" },
  { value: "adelantado", text: "Mes adelantado" },
];

export const typeOfBuildingOptions = [
  { value: "H", text: "Consorcio de propietarios" },
  { value: "R", text: "Edificio de renta - Propietario único" },
];

export const penaltyInterestsModeOptions = [
  { value: "capital", text: "Sobre capital" },
  { value: "interest", text: "Interés s/Interés" },
];

export const booleanOptions = [
  { value: true, text: "Si" },
  { value: false, text: "No" },
];

export const paymentMethodOptions = [
  { value: null, text: "Ninguno" },
  { value: 1, text: "Pago Facíl" },
  { value: 2, text: "Siro" },
  { value: 3, text: "Pago mis Expensas" },
  { value: 4, text: "Expensas pagas" },
  { value: 5, text: "Interfast" },
];

export const headerOpts = [
  { value: 1, text: "Por defecto" },
  { value: 2, text: "Todas las cabecera creadas" },
];

export const providerSpendingLocationOpts = [
  { value: "delante", text: "Mostrar delante de gastos" },
  { value: "detras", text: "Mostrar detras de gastos" },
  { value: null, text: "No mostrar" },
];

export const roundingPayslipsOptions = [
  { value: true, text: "Con redondeo" },
  { value: false, text: "Sin redondeo" },
];

export const headerFiscalSituations = [
  { value: "responsable_no_inscripto", text: "Responsable no inscripto" },
  { value: "responsable_inscripto", text: "Responsable inscripto" },
  { value: "monotributista", text: "Monotributista" },
  { value: "exento", text: "Exento" },
];

export const contactType = [
  { value: "PROPIETARIO", text: "Propietario" },
  { value: "INQUILINO", text: "Inquilino" },
  { value: "OTRO", text: "Otro" },
];

export const documentType = [
  { value: "DNI", text: "DNI" },
  { value: "LC", text: "Libreta Cívica" },
  { value: "LE", text: "Libreta de Enrolamiento" },
  { value: "PASAPORTE", text: "Pasaporte" },
];

export const bankAccountType = [
  { value: "current_account", text: "Cuenta Corriente" },
  { value: "savings_box", text: "Caja Ahorro" },
  { value: "single_account", text: "Cuenta Única" },
];

export const fiscalSituationsProviders = [
  { value: null, text: "-" },
  { value: "responsable_no_inscripto", text: "Responsable no Inscripto" },
  { value: "responsable_inscripto", text: "Responsable Inscripto" },
  { value: "monotributista", text: "Monotributista" },
  { value: "exento", text: "Exento" },
];
export const paymentOrderTypes = [
  { value: "provider", text: "Proveedor"},
  // { value: "consortium", text: "Consorcio"},
]


export const incomeTypeOptions = [
  { value: true, text: "Otros Ingresos del Consorcio" },
  { value: false, text: "Depósito sin Identificar" },
];
