import router from "../../router";
import AuthRepository from '../../repositories/AuthRepository'
import {UserModel} from '../../models/UserModel'

export default {
  state: {
    currentUser:
      localStorage.getItem("user") != null
        ? new UserModel(JSON.parse(localStorage.getItem("user"))) 
        : null,
    loginError: null,
    processing: false,
    forgotMailSuccess: null,
    resetPasswordSuccess: null
  },
  getters: {
    currentUser: state => state.currentUser,
    processing: state => state.processing,
    loginError: state => state.loginError,
    forgotMailSuccess: state => state.forgotMailSuccess,
    resetPasswordSuccess: state => state.resetPasswordSuccess
  },
  mutations: {
    setUser(state, payload) {
      state.currentUser = new UserModel(payload);
      state.processing = false;
      state.loginError = null;
    },
    setLogout(state) {
      state.currentUser = null;
      state.processing = false;
      state.loginError = null;
    },
    setProcessing(state, payload) {
      state.processing = payload;
      state.loginError = null;
    },
    setError(state, payload) {
      state.loginError = payload;
      state.currentUser = null;
      state.processing = false;
    },
    setForgotMailSuccess(state) {
      state.loginError = null;
      state.currentUser = null;
      state.processing = false;
      state.forgotMailSuccess = true;
    },
    setResetPasswordSuccess(state) {
      state.loginError = null;
      state.currentUser = null;
      state.processing = false;
      state.resetPasswordSuccess = true;
    },
    clearError(state) {
      state.loginError = null;
    }
  },
  actions: {
    async login({ commit }, userData) {
      commit("clearError");
      commit("setProcessing", true);
      try {
       
        let user = await AuthRepository.login(userData)
        localStorage.setItem("user", JSON.stringify(user));
        commit("setUser", { ...user });
        router.push({ path: "/" });
      } catch (err) {
        localStorage.removeItem("user");
        commit("setError", err.message);
        setTimeout(() => {
          commit("clearError");
        }, 3000);
      }
    },
    async signOut({ commit }, auth) {
      try {
       
        await AuthRepository.logout(auth)
        localStorage.removeItem("user");
        router.push({ path: "/user/login" });
      } catch (err) {
        commit("setError", err.message);
        setTimeout(() => {
          commit("clearError");
        }, 3000);
      }
    },
    forgotPassword({ commit }, payload) {
      commit("clearError");
      commit("setProcessing", true);
      /*Forgot Password */
    },
    resetPassword({ commit }, payload) {
      commit("clearError");
      commit("setProcessing", true);
      /*Reset Password */
    }
  }
};
