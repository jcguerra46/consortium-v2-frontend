import LocationsRepository from "../../repositories/LocationsRepository";

const state = {
  countries: [],
  provinces: [],
};

const mutations = {
  setCountries(state, payload) {
    state.countries = payload;
  },

  setProvinces(state, payload) {
    state.provinces = payload;
  },
};

const actions = {
  async getCountries({ commit }, auth) {
    let countries = await LocationsRepository.getCountries(auth);

    commit("setCountries", countries);
  },

  async getProvinces({ commit }, data) {
    const { auth, countryId } = data;
    let provinces = await LocationsRepository.getProvinces(auth, countryId);

    commit("setProvinces", provinces);
  },
};

export default {
  state,
  mutations,
  actions
};
