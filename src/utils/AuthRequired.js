import Axios from "axios";
const BASEURL = process.env.VUE_APP_BASEURL;
import AuthRepository from '../repositories/AuthRepository'
import { UserModel } from "../models/UserModel";


export default async (to, from, next) => {
  if (
    localStorage.getItem("user") != null &&
    localStorage.getItem("user").length > 0
  ) {
    try {
      let user =new UserModel(JSON.parse(localStorage.getItem("user"))) 
      await AuthRepository.isLoggedIn(user.authHeader)
      next();
    } catch (err) {
      localStorage.removeItem("user");
      next("/user/login");
    }
  } else {
    localStorage.removeItem("user");
    next("/user/login");
  }
};
