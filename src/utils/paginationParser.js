export const paginationParser = (response) => {
  let data = response.data;
  let { total, total_pages, per_page, current_page } = data.metadata;
  let from = per_page * (current_page - 1) + 1;
  let to = per_page * current_page
  if(to > total) { to = total }
  const parsedData = {
    total,
    last_page: total_pages,
    per_page,
    current_page,
    from,
    to,

    data: data.rows,
  };

  return parsedData;
};
