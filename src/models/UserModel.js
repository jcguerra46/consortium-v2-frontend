export class UserModel {
  constructor(rawData) {
    this.id = rawData.id;
    this.administrationId = rawData.administration_id;
    this.principal = rawData.principal;
    this.firstName = rawData.first_name
    this.lastName = rawData.last_name
    this.email = rawData.email
    this.menu = rawData.menu
    this.token = rawData.token
  }
  get fullName() {
    return `${this.name} ${this.lastName}`;
  }
  get authHeader() {
    return {
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    };
  }
}
