import Vue from "vue";
import VueRouter from "vue-router";
import AuthRequired from "./utils/AuthRequired";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: () => import(/* webpackChunkName: "app" */ "./views/app"),
    redirect: "/app",
    beforeEnter: AuthRequired,
    children: [
      {
        path: "app",
        component: () =>
          import(/* webpackChunskName: "home" */ "./views/app/home"),
        redirect: "/app/consortiums",
        children: [
          {
            //--------Consorcios---------
            path: "consortiums",
            component: () =>
              import(/* webpackChunkName: "home" */ "./views/app/home/index"),
            children: [
              {
                path: "/",
                name: "consortium-list",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "create",
                name: "consortium-create",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/ProfileConsortium"
                  ),
              },
              {
                path: "create/wizard",
                name: "consortium-wizard",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/CreateConsortium"
                  ),
              },
              {
                path: ":id/profile",
                name: "consortium-profile",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/ProfileConsortium"
                  ),
              },
              {
                path: ":id/functional-units",

                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/functional-units/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "functional-units-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/functional-units/FunctionalUnitsList"
                      ),
                  },
                  {
                    path: ":idFU/profile",
                    name: "functional-units-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/functional-units/FunctionalUnitsProfile"
                      ),
                  },
                  {
                    path: ":idFU/contacts",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/functional-units/Contacts/index"
                      ),
                    children: [
                      {
                        path: "/",
                        name: "functional-units-contacts-list",
                        component: () =>
                          import(
                            /* webpackChunkName: "home" */ "./views/app/home/functional-units/Contacts/ContactsList"
                          ),
                      },
                      {
                        path: "create",
                        name: "functional-units-contacts-create",
                        component: () =>
                          import(
                            /* webpackChunkName: "home" */ "./views/app/home/functional-units/Contacts/ContactsCreate"
                          ),
                      },
                      {
                        path: ":idContact/edit",
                        name: "functional-units-contacts-edit",
                        component: () =>
                          import(
                            /* webpackChunkName: "home" */ "./views/app/home/functional-units/Contacts/ContactsEdit"
                          ),
                      },
                    ],
                  },
                ],
              },
              {
                path: ":id/percentage",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/ConsortiumPercentage/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "consortium-percentage-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/ConsortiumPercentage/ConsortiumPercentageList"
                      ),
                  },
                  {
                    path: "create",
                    name: "consortium-percentage-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/ConsortiumPercentage/ConsortiumPercentageCreate"
                      ),
                  },
                  {
                    path: ":idPercentage/edit",
                    name: "consortium-percentage-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/ConsortiumPercentage/ConsortiumPercentageEdit"
                      ),
                  },
                ],
              },
            ],
          },
          {
            //--------Gastos---------
            path: "spendings",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/spendings/index"
              ),
            redirect: "/app/spendings/list",
            children: [
              {
                path: "list",
                name: "spending-list",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/SpendingList.vue"
                  ),
              },
              {
                path: "create",
                name: "spending-create",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/SpendingCreate.vue"
                  ),
              },
              {
                path: ":id/edit",
                name: "spending-edit",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/SpendingEdit.vue"
                  ),
              },
              {
                path: "recurrents",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/recurrents/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "spending-recurrent-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/recurrents/SpendingRecurrentsList.vue"
                      ),
                  },
                  {
                    path: "create",
                    name: "spending-recurrent-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/recurrents/SpendingRecurrentsCreate.vue"
                      ),
                  },
                  {
                    path: ":id/edit",
                    name: "spending-recurrent-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/recurrents/SpendingRecurrentsEdit.vue"
                      ),
                  },
                ],
              },
              {
                path: "payment-orders",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/payment-orders/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "payment-orders-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/payment-orders/PaymentOrdersList.vue"
                      ),
                  },
                  {
                    path: "create",
                    name: "payment-orders-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/payment-orders/PaymentOrdersCreate.vue"
                      ),
                  },
                  {
                    path: ":id/edit",
                    name: "payment-orders-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/payment-orders/PaymentOrdersEdit.vue"
                      ),
                  },
                  {
                    path: ":id/payment",
                    name: "payment-orders-payment",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/payment-orders/PaymentOrdersPayment.vue"
                      ),
                  },
                ],
              },
              {
                path: "descriptions",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/descriptions/index.vue"
                  ),
                children: [
                  {
                    path: "/",
                    name: "spending-descriptions-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/descriptions/SpendingDescriptionsList.vue"
                      ),
                  },
                  {
                    path: "create",
                    name: "spending-descriptions-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/descriptions/SpendingDescriptionsCreate.vue"
                      ),
                  },
                  {
                    path: ":id/edit",
                    name: "spending-descriptions-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/descriptions/SpendingDescriptionsEdit.vue"
                      ),
                  },
                ],
              },
              {
                path: "providers",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/spendings/index.vue"
                  ),
                redirect: "/app/spendings/providers/list",
                children: [
                  {
                    path: "list",
                    name: "list-provider",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/providers/ProvidersList"
                      ),
                  },
                  {
                    path: "create",
                    name: "create-provider",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/providers/ProviderCreate"
                      ),
                  },
                  {
                    path: "edit/:idProvider",
                    name: "edit-provider",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/providers/ProviderEdit"
                      ),
                  },
                  {
                    path: "current-account",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/spendings/providers/current-account-providers/CurrentAccountProviderList"
                      ),
                  },
                ],
              },
            ],
          },
          {
            //--------Cobranzas---------
            path: "collections",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/home/Consortium"
              ),
          },
          {
            //--------Sueldos---------
            path: "salaries",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/salaries/index"
              ),
            children: [
              {
                path: "employees",
                redirect: "employees/list",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/salaries/employees/index"
                  ),
                children: [
                  {
                    path: "list",
                    name: "employees",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/salaries/employees/EmployeesList"
                      ),
                  },
                  {
                    path: "create",
                    name: "employee-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/salaries/employees/EmployeesList"
                      ),
                  },
                ],
              },
            ],
          },
          {
            //--------Caja---------
            path: "cashbox",
            redirect: "cashbox/cash-count",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/cashbox/index"
              ),
            children: [
              {
                path: "cash-count",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/cashbox/CashboxList"
                  ),
              },
              {
                path: "financial-statement",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/cashbox/financial-statement/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "financial-statement-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/financial-statement/FinancialStatementList"
                      ),
                  },
                  {
                    path: "create-income",
                    name: "financial-statement-income-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/financial-statement/FinancialStatementIncomeCreate"
                      ),
                  },
                  {
                    path: "create-outflow",
                    name: "financial-statement-outflow-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/financial-statement/FinancialStatementOutflowCreate"
                      ),
                  },
                ],
              },
              {
                path: "others-cashboxes",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/cashbox/others-cashboxes/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "others-cashboxes",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/others-cashboxes/OthersCashboxes"
                      ),
                  },
                  {
                    path: "create",
                    name: "others-cashboxes-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/others-cashboxes/OthersCashboxesCreate"
                      ),
                  },
                  {
                    path: ":idConsortium/edit/:idCashbox",
                    name: "others-cashboxes-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/others-cashboxes/OthersCashboxesEdit"
                      ),
                  },
                  {
                    path: ":idConsortium/transfer/:idCashbox",
                    name: "others-cashboxes-transfer",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/others-cashboxes/OthersCashboxesTransfer"
                      ),
                  },
                  {
                    path: "movements",
                    name: "others-cashboxes-movements",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/others-cashboxes/OthersCashboxesListMovements"
                      ),
                  },
                ],
              },
              {
                path: "checks",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
                children: [
                  {
                    path: "checkbooks",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                      ),
                  },
                ],
              },
              {
                path: "bank-accounts",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/cashbox/bank-accounts/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "bank-accounts",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/bank-accounts/BankAccountsList"
                      ),
                  },
                  {
                    path: "create",
                    name: "bank-account-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/bank-accounts/BankAccountCreate"
                      ),
                  },
                  {
                    path: "profile",
                    name: "bank-account-profile",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/bank-accounts/BankAccountEdit"
                      ),
                  },
                  {
                    path: "movements",
                    name: "bank-account-movements",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/cashbox/bank-accounts/BankAccountsListMovements"
                      ),
                  },
                ],
              },
            ],
          },
          {
            //--------Unidades Funcionales---------
            path: "functional-units",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/home/functional-units/index"
              ),
            children: [
              {
                path: "current-account",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/functional-units/CurrentAccount/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "functional-units-cc",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/functional-units/CurrentAccount/FunctionalUnitsCurrentAccount"
                      ),
                  },
                  {
                    path: ":fUId/register-debt",
                    name: "functional-units-register-debt",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/functional-units/CurrentAccount/RegisterDebt"
                      ),
                  },
                  {
                    path: ":fUId/forgive-debt",
                    name: "functional-units-forgive-debt",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/home/functional-units/CurrentAccount/ForgiveDebt"
                      ),
                  },
                ],
              },
              {
                path: "contacts",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "claims",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
            ],
          },
          {
            //--------reports---------
            path: "reports",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/home/Consortium"
              ),
            children: [
              {
                path: "others",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "user-cashboxes",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "audit",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
            ],
          },
          {
            //--------Configuraciones---------
            path: "configurations",
            component: () =>
              import(
                /* webpackChunkName: "home" */ "./views/app/configurations/index"
              ),
            children: [
              {
                path: "periods",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "headers",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/configurations/headers/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "headers-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/configurations/headers/HeaderList"
                      ),
                  },
                  {
                    path: "create",
                    name: "headers-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/configurations/headers/HeaderCreate"
                      ),
                  },
                  {
                    path: ":id/edit",
                    name: "headers-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/configurations/headers/HeaderEdit"
                      ),
                  },
                ],
              },
              {
                path: "notes",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "trials",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "categories",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/home/Consortium"
                  ),
              },
              {
                path: "services",
                component: () =>
                  import(
                    /* webpackChunkName: "home" */ "./views/app/configurations/services/index"
                  ),
                children: [
                  {
                    path: "/",
                    name: "services-list",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/configurations/services/ServiceList"
                      ),
                  },
                  {
                    path: "crear",
                    name: "services-create",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/configurations/services/ServiceCreate"
                      ),
                  },
                  {
                    path: ":id/editar",
                    name: "services-edit",
                    component: () =>
                      import(
                        /* webpackChunkName: "home" */ "./views/app/configurations/services/ServiceEdit"
                      ),
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    path: "/error",
    component: () => import(/* webpackChunkName: "error" */ "./views/Error"),
  },
  {
    path: "/user",
    component: () => import(/* webpackChunkName: "user" */ "./views/user"),
    redirect: "/user/login",
    children: [
      {
        path: "login",
        component: () =>
          import(/* webpackChunkName: "user" */ "./views/user/Login"),
      },
      {
        path: "reset-password",
        component: () =>
          import(/* webpackChunkName: "user" */ "./views/user/ResetPassword"),
      },
      {
        path: "forgot-password",
        component: () =>
          import(/* webpackChunkName: "user" */ "./views/user/ForgotPassword"),
      },
    ],
  },
  {
    path: "*",
    component: () => import(/* webpackChunkName: "error" */ "./views/Error"),
  },
];

const router = new VueRouter({
  linkActiveClass: "active",
  routes,
});

export default router;
